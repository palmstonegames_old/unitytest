using System.Drawing;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace UnityTest
{
    [InitializeOnLoad]
    public static class Icons
    {
        public static Texture2D FailImg;
        public static Texture2D IgnoreImg;
        public static Texture2D RunImg;
        public static Texture2D RunFailedImg;
        public static Texture2D RunAllImg;
        public static Texture2D SuccessImg;
        public static Texture2D UnknownImg;
        public static Texture2D InconclusiveImg;
        public static Texture2D StopwatchImg;
        public static Texture2D PlusImg;
        public static Texture2D GearImg;

        public static GUIContent GUIUnknownImg;
        public static GUIContent GUIInconclusiveImg;
        public static GUIContent GUIIgnoreImg;
        public static GUIContent GUISuccessImg;
        public static GUIContent GUIFailImg;
        public static GUIContent GUIOptionsHideLabel;
        public static GUIContent GUIOptionsShowLabel;
        public static GUIContent GUICreateNewTest;
        public static GUIContent GUIRunSelectedTests;
        public static GUIContent GUIRunAllTests;
        public static GUIContent GUIAdvancedFilterShow;
        public static GUIContent GUIAdvancedFilterHide;
        public static GUIContent GUITimeoutIcon;
        public static GUIContent GUIRunSelected;
        public static GUIContent GUIRun;
        public static GUIContent GUIRunAll;
        public static GUIContent GUIRunAllIncludingIgnored;
        public static GUIContent GUIDelete;
        public static GUIContent GUIAddGoUderTest;
        public static GUIContent GUIBlockUi;
        public static GUIContent GUIHideTestInHierarchy;
        public static GUIContent GUIHideTestRunner;
        public static GUIContent GUIRunSelectedTestsIcon;
        public static GUIContent GUIRunAllTestsIcon;
        public static GUIContent GUIRerunFailedTestsIcon;
        public static GUIContent GUIOptionButton;
        public static GUIContent GUIRunOnRecompile;
        public static GUIContent GUIRunTestsOnNewScene;
        public static GUIContent GUIAutoSaveSceneBeforeRun;
        public static GUIContent GUIShowDetailsBelowTests;
        public static GUIContent GUINotifyWhenSlow;
        public static GUIContent GUISlowTestThreshold;
        public static GUIContent GUIOpenInEditor;
        public static GUIContent GUIHideGroup;
        public static GUIContent GUIUnhideGroup;
        public static GUIContent GUIStopWatch;

        private static string _scene;

        static Icons()
        {
            Load();
            EditorApplication.playmodeStateChanged += Load;
            EditorApplication.hierarchyWindowChanged += () =>
            {
                if (_scene != EditorApplication.currentScene)
                {
                    Load();
                }
            };
        }

        private static void Load()
        {
            _scene = EditorApplication.currentScene;
            FailImg = LoadTexture(IconData.failed);
            IgnoreImg = LoadTexture(IconData.ignored);
            RunImg = LoadTexture(EditorGUIUtility.isProSkin ? IconData.play_selected_darktheme : IconData.play_selected_lighttheme);
            RunFailedImg = LoadTexture(EditorGUIUtility.isProSkin ? IconData.rerun_darktheme : IconData.rerun_lighttheme);
            RunAllImg = LoadTexture(EditorGUIUtility.isProSkin ? IconData.play_darktheme : IconData.play_lighttheme);
            SuccessImg = LoadTexture(IconData.passed);
            UnknownImg = LoadTexture(IconData.normal);
            InconclusiveImg = LoadTexture(IconData.inconclusive);
            StopwatchImg = LoadTexture(IconData.stopwatch);
            PlusImg = LoadTexture(EditorGUIUtility.isProSkin ? IconData.create_darktheme : IconData.create_lighttheme);
            GearImg = LoadTexture(EditorGUIUtility.isProSkin ? IconData.options_darktheme : IconData.options_lighttheme);
            GUIUnknownImg = new GUIContent(UnknownImg);
            GUIInconclusiveImg = new GUIContent(InconclusiveImg);
            GUIIgnoreImg = new GUIContent(IgnoreImg);
            GUISuccessImg = new GUIContent(SuccessImg);
            GUIFailImg = new GUIContent(FailImg);
            GUIOptionsHideLabel = new GUIContent("Hide", Icons.GearImg);
            GUIOptionsShowLabel = new GUIContent("Options", Icons.GearImg);
            GUICreateNewTest = new GUIContent(Icons.PlusImg, "Create new test");
            GUIRunSelectedTests = new GUIContent(Icons.RunImg, "Run selected test(s)");
            GUIRunAllTests = new GUIContent(Icons.RunAllImg, "Run all tests");
            GUIAdvancedFilterShow = new GUIContent("Advanced");
            GUIAdvancedFilterHide = new GUIContent("Hide");
            GUITimeoutIcon = new GUIContent(Icons.StopwatchImg, "Timeout");
            GUIRunSelected = new GUIContent("Run selected");
            GUIRun = new GUIContent("Run");
            GUIRunAll = new GUIContent("Run All");
            GUIRunAllIncludingIgnored = new GUIContent("Run All (include ignored)");
            GUIDelete = new GUIContent("Delete");
            GUIAddGoUderTest = new GUIContent("Add GOs under test", "Add new GameObject under selected test");
            GUIBlockUi = new GUIContent("Block UI when running", "Block UI when running tests");
            GUIHideTestInHierarchy = new GUIContent("Hide tests in hierarchy", "Hide tests in hierarchy");
            GUIHideTestRunner = new GUIContent("Hide Test Runner", "Hides Test Runner object in hierarchy");
            GUIRunSelectedTestsIcon = new GUIContent(Icons.RunImg, "Run selected tests");
            GUIRunAllTestsIcon = new GUIContent(Icons.RunAllImg, "Run all tests");
            GUIRerunFailedTestsIcon = new GUIContent(Icons.RunFailedImg, "Rerun failed tests");
            GUIOptionButton = new GUIContent("Options", Icons.GearImg);
            GUIRunOnRecompile = new GUIContent("Run on recompile", "Run on recompile");
            GUIRunTestsOnNewScene = new GUIContent("Run tests on a new scene", "Run tests on a new scene");
            GUIAutoSaveSceneBeforeRun = new GUIContent("Autosave scene", "The runner will automaticall save current scene changes before it starts");
            GUIShowDetailsBelowTests = new GUIContent("Show details below tests", "Show run details below test list");
            GUINotifyWhenSlow = new GUIContent("Notify when test is slow", "When test will run longer that set threshold, it will be marked as slow");
            GUISlowTestThreshold = new GUIContent("Slow test threshold");
            GUIOpenInEditor = new GUIContent("Open in editor");
            GUIHideGroup = new GUIContent("Hide group");
            GUIUnhideGroup = new GUIContent("Unhide group");
            GUIStopWatch = new GUIContent(Icons.StopwatchImg, "Test run too long");
        }

        public static Texture2D LoadTexture(Bitmap bitmap)
        {
            var tex = new Texture2D(bitmap.Width, bitmap.Width);
            using (var stream = new MemoryStream())
            {
                bitmap.Save(stream, bitmap.RawFormat);
                tex.LoadImage(stream.ToArray());
            }

            return tex;
        }
    }
}